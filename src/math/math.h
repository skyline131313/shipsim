
#ifndef ZZ_MATH_MATH_H
#define ZZ_MATH_MATH_H

#include "../math/matrix.h"
#include "../math/vector.h"
#include "../math/transform.h"
#include "../math/aabb.h"

#include "../common/type.h"

#include <type_traits>
#include <limits>

namespace zz
{


namespace Math
{
    template<typename T>
    T Pi()
    {
        static_assert(std::is_floating_point<T>::value,         "pi() requires floating point precision.");
        static_assert(std::numeric_limits<T>::digits10 <= 50,   "pi() needs to be modified to support greater precision.");

        return T(3.1415926535897932384626433832795028841971693993751058);
    }
}


}

#endif // guard