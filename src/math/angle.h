
#ifndef ZZ_MATH_ANGLE_H
#define ZZ_MATH_ANGLE_H

#include "vector.h"

#include <cmath>

namespace zz
{

class Angle
{
public:

             Angle() { }
    explicit Angle(f32 radians)                 { Set(radians); }

    f32     ComputeAngle() const                { return std::atan2(sin, cos); }

    void    Set(f32 radians)                    { cos = std::cos(radians); sin = std::sin(radians); }
    void    SetIdentity()                       { cos = 1.0f; sin = 0.0f; }

    Vec2    GetDirectionNormal() const          { return Vec2(cos, sin); }
    Vec2    RotatePoint(const Vec2& p) const    { return Vec2(p.x*cos+p.y*(-sin), p.x*sin+p.y*cos); }
    

private:

    f32 cos;
    f32 sin;

};

}

#endif // guard