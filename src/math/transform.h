
#ifndef ZZ_MATH_TRANSFORM_H
#define ZZ_MATH_TRANSFORM_H

#include "vector.h"
#include "angle.h"

namespace zz
{

class Transform
{
public:

                    Transform() { }
                    Transform(const Vec2& pos, const Angle& rot) : position(pos), rotation(rot) { }

    const Vec2&     GetPosition() const { return position; }
    const Angle&    GetRotation() const { return rotation; }

    void            Set(const Vec2& pos, const Angle& rot)  { position = pos; rotation = rot; }
    void            SetIdentity() { position.SetZero(); rotation.SetIdentity(); }

    Vec2            TransformPoint(const Vec2& p) const { return rotation.RotatePoint(p) + position; }
    Vec2            RotatePoint(const Vec2& p) const { return rotation.RotatePoint(p); }

private:

    Vec2    position;
    Angle   rotation;

};

}

#endif // guard