
#ifndef ZZ_MATH_VECTOR_H
#define ZZ_MATH_VECTOR_H

#include "../common/type.h"

#include <cmath>

namespace zz
{

class Vec2
{
public:

    f32     x;
    f32     y;

    Vec2() { }
    Vec2(f32 x, f32 y) : x(x), y(y) { }
    
    Vec2    operator -  () const { return Vec2(-x,-y); }

    f32     operator *  (const Vec2& a) const { return x*a.x+y*a.y; }               //!< vector dot product
    Vec2    operator +  (const Vec2& a) const { return Vec2(x+a.x, y+a.y); }
    Vec2    operator -  (const Vec2& a) const { return Vec2(x-a.x, y-a.y); }
    Vec2&   operator += (const Vec2& a)       { x += a.x; y += a.y; return *this; }
    Vec2&   operator -= (const Vec2& a)       { x -= a.x; y -= a.y; return *this; }

    Vec2    operator *  (f32 a) const { return Vec2(x*a, y*a); }
    Vec2&   operator *= (f32 a)       { x *= a; y *= a; return *this; }
    Vec2    operator /  (f32 a) const { f32 i = 1/a; return *this * i; }
    Vec2&   operator /= (f32 a)       { f32 i = 1/a; return *this *= i; }

    void    SetZero()           { x = 0;  y = 0;  }
    void    Set(f32 x_, f32 y_) { x = x_; y = y_; } 

    f32     Normalize(); //!< \returns Vec2::Length() prior to normalization.
    
    f32     LengthSqr() const   { return x*x+y*y; }
    f32     Length() const      { return std::sqrt(LengthSqr()); }

    f32     Dot(const Vec2& a) const        { return (*this) * a; }
    f32     PerpDot(const Vec2& a) const    { return x * a.y - y * a.x; } //!< Positive if on left side, Negative if on right side of this
    Vec2    Perp() const                    { return Vec2(-y, x); } //!< \returns a perpendicular vector to the current one
};

static_assert(sizeof(Vec2) == sizeof(f32)*2, "Vec2 unexpected size."); // make sure we don't get unwanted padding...

inline Vec2 operator * (f32 a, const Vec2& b) { return b*a; }
inline Vec2 operator / (f32 a, const Vec2& b) { return Vec2(a/b.x, a/b.y); }

inline f32 Vec2::Normalize()
{
    f32 len = Length();
    *this /= len;
    return len;
}

} // ns zz

#endif // guard