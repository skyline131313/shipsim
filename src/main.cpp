
#include "core/core.h"
#include "core/renderinfo.h"

#include "physics/isolatesystem.h"
#include "physics/shapes/circle.h"
#include "physics/shapes/convex.h"

const float scale = 1 / 18.0f;

using namespace zz;

class SfmlRenderer : public PhysicsDebugRenderer
{
public:

    SfmlRenderer() : target(nullptr) { font.loadFromFile("data/whitrabt.ttf"); }

    void LendRenderTarget(sf::RenderTarget* t) { target = t; }

    virtual void DrawText(const Vec2& pos, const std::string& str) const override
    {
        if(target == nullptr) return;

        sf::Text text;

        text.setCharacterSize(16);
        text.setFont(font);
        text.setPosition(pos.x, pos.y);
        text.setString(str);
        text.setScale(scale, scale);

        target->draw(text);

    }

    virtual void DrawContact(const Vec2& pos, const Vec2& normal) const override
    {
        if(target == nullptr) return;

        const float w = 1.0f;

        sf::RectangleShape s;

        s.setSize(sf::Vector2f(w,w));
        s.setOrigin(w/2,w/2);
        s.setPosition(pos.x, pos.y);
        s.setFillColor(sf::Color::Red);

        target->draw(s);

        DrawEdge(pos, pos+normal);

    }

    virtual void DrawCircle(const Transform& transform, f32 radius) const override
    {
        if(target == nullptr) return;

        Vec2 pos = transform.GetPosition();
        Vec2 end = transform.GetRotation().GetDirectionNormal() * radius + pos;
        
        sf::CircleShape s;

        s.setFillColor(sf::Color::Transparent);
        s.setOutlineThickness(-0.2f);
        s.setRadius(radius);
        s.setPosition(pos.x, pos.y);
        s.setOrigin(radius, radius);
        target->draw(s);

        sf::VertexArray v(sf::PrimitiveType::Lines, 2);

        v[0].position = sf::Vector2f(pos.x, pos.y);
        v[1].position = sf::Vector2f(end.x, end.y);

        target->draw(v);
    }

    virtual void DrawEdge(const Vec2& pt0, const Vec2& pt1) const override
    {
        if(target == nullptr) return;

        sf::VertexArray v(sf::PrimitiveType::Lines, 2);

        v[0].position = sf::Vector2f(pt0.x, pt0.y);
        v[1].position = sf::Vector2f(pt1.x, pt1.y);

        target->draw(v);

    }

private:

    sf::RenderTarget* target;
    sf::Font font;
};

int main()
{
    const Vec2 gravity = Vec2(0, 0);
    const int frameRate = 60;
    const f32 deltaTime = 1.0f / frameRate;
    

    sf::RenderWindow window(sf::VideoMode(800, 600), "Game");
    sf::View cam(sf::Vector2f(0,0), sf::Vector2f(800 * scale, 600 * scale));
    
    window.setFramerateLimit(frameRate);
    window.setView(cam);
    
    IsolateSystem system(gravity);
    SfmlRenderer renderer;
    
    renderer.LendRenderTarget(&window);

    Body* body;

    {
        Body::Properties prop;

        auto c = Convex::Factory::Create();

        Vec2 p[] = {
            Vec2(0, 0), Vec2(2, 2), Vec2(0,2), Vec2(2,0), Vec2(1, -6)
        };

        c->Set(p, 4);
        //c->Set(3.0f);

        prop.shape = c.get();
        prop.type = Body::typeDynamic;
        prop.density = 1.0f;
        prop.restitution = 1.0f;
        prop.position = Vec2(1.0f, 20);
        prop.velocity = Vec2(0, -10.5f);
        prop.rotation = 0;
        //prop.angularVelocity = Math::Pi<f32>() / 6;

        system.AddBody(prop);

        prop.position = Vec2(0, -20);
        prop.velocity = Vec2(0, 10.5f);
        //prop.rotation = zz::Math::Pi<f32>() / 4;
        //prop.angularVelocity = -prop.angularVelocity;

        body = system.AddBody(prop);
    }

    bool manual = false;
    

    while(window.isOpen())
    {

        bool step = false;

        sf::Event ev;
        while(window.pollEvent(ev))
        {
            switch(ev.type)
            {
            case sf::Event::Closed: window.close(); break;
            case sf::Event::KeyPressed:
                switch(ev.key.code)
                {
                case sf::Keyboard::M:
                    manual = !manual;
                    break;
                case sf::Keyboard::N:
                    step = true;
                    break;
                }
                break;
            }
        }

        if(!manual || (manual && step)) system.Step(deltaTime, 1);
   
#if 0
        const Transform& tf = body->GetTransform();

        cam.setRotation( tf.GetRotation().ComputeAngle() * (180 / Math::Pi<f32>()) );
        cam.setCenter(tf.GetPosition().x, tf.GetPosition().y);

        window.setView(cam);
#endif

        window.clear(sf::Color(70,70,70));
        system.Draw(renderer);
        window.display();
    }

    return 0;
}