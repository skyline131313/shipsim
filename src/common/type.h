
#ifndef ZZ_COMMON_TYPE_H
#define ZZ_COMMON_TYPE_H

#include <cstdint>

namespace zz
{

typedef float  f32;
typedef double f64;

typedef int8_t  s8;     static_assert(sizeof(s8)  == 1, "s8  is not 8  bits");
typedef int16_t s16;    static_assert(sizeof(s16) == 2, "s16 is not 16 bits");
typedef int32_t s32;    static_assert(sizeof(s32) == 4, "s32 is not 32 bits");
typedef int64_t s64;    static_assert(sizeof(s64) == 8, "s64 is not 64 bits");

typedef uint8_t  u8;    static_assert(sizeof(u8)  == 1, "u8  is not 8  bits");
typedef uint16_t u16;   static_assert(sizeof(u16) == 2, "u16 is not 16 bits");
typedef uint32_t u32;   static_assert(sizeof(u32) == 4, "u32 is not 32 bits");
typedef uint64_t u64;   static_assert(sizeof(u64) == 8, "u64 is not 64 bits");



} // ns zz

#endif // guard