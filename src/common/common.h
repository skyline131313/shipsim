
#ifndef ZZ_COMMON_COMMON_H
#define ZZ_COMMON_COMMON_H

#include <cmath>

#include <list>
#include <memory>
#include <string>

#include "type.h"

#include "../math/math.h"

namespace zz
{

template<typename T>
T WrapIndex(T i, T num)
{
    return (i < num ? i : 0);
}

class NonCopyable
{
protected:

    NonCopyable() { }
    ~NonCopyable() { }
    
private:
    NonCopyable(const NonCopyable&) { }
    NonCopyable& operator = (const NonCopyable&) { }

};

}

#endif