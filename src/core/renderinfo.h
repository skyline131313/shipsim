
#ifndef ZZ_CORE_RENDERINFO_H
#define ZZ_CORE_RENDERINFO_H

#include "core.h"

namespace zz
{

struct RenderInfo
{
    sf::RenderTarget* target;
};

} // ns ss

#endif // guard