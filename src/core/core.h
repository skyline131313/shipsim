
#ifndef ZZ_CORE_CORE_H
#define ZZ_CORE_CORE_H


#define SFML_STATIC
#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#if defined(_DEBUG) || defined(DEBUG)
    #pragma comment(lib,"sfml-graphics-s-d.lib")
    #pragma comment(lib,"sfml-audio-s-d.lib")
    #pragma comment(lib,"sfml-network-s-d.lib")
    #pragma comment(lib,"sfml-window-s-d.lib")
    #pragma comment(lib,"sfml-system-s-d.lib")
    #pragma comment(lib,"sfml-main-d.lib")
#else
    #pragma comment(lib,"sfml-graphics-s.lib")
    #pragma comment(lib,"sfml-audio-s.lib")
    #pragma comment(lib,"sfml-network-s.lib")
    #pragma comment(lib,"sfml-window-s.lib")
    #pragma comment(lib,"sfml-system-s.lib")
    #pragma comment(lib,"sfml-main.lib")
#endif

namespace zz
{

class Game
{
public:

    static int toTicks(int ms) { return ms / logicUpdateTime; }

private:

    static const int frameRate = 40;
    static const int logicUpdateTime = 1000/frameRate;

};


} // ns ss

#endif // guard