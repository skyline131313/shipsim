
#include "body.h"

namespace zz
{

void Body::ManifoldApplyImpulse(Manifold& m)
{
    Body& b1 = *m.body1;
    Body& b2 = *m.body2;

    for(s32 i = 0; i < m.contactsNum; ++i)
    {

        Vec2 displacement1 = m.contacts[i] - b1.transform.GetPosition();
        Vec2 displacement2 = m.contacts[i] - b2.transform.GetPosition();

        Vec2 relative = b2.velocity + b2.angularVelocity * displacement2.Perp()
                      - b1.velocity - b1.angularVelocity * displacement1.Perp();
        f32  speed = relative * m.normal;

        if(speed > 0) return;

        f32 j = (-(1.0f + m.restitution) * speed) / (b1.massInverse + b2.massInverse);
        j /= m.contactsNum;

        Vec2 impulse = j * m.normal;

        b1.velocity -= b1.massInverse * impulse;
        b1.angularVelocity -= b1.inertiaInverse * displacement1.PerpDot(impulse);

        b2.velocity += b2.massInverse * impulse;
        b2.angularVelocity += b2.inertiaInverse * displacement2.PerpDot(impulse);

    }

}

Body::Body(IsolateSystem* system, const Properties& prop)
    : isolateSystem(system)
{

    shape = prop.shape->Clone();

    type = prop.type;
    restitution = prop.restitution;
    density = prop.density;

    transform.Set(prop.position, Angle(prop.rotation));

    if(type == typeDynamic)
    {
        velocity = prop.velocity;
        angularVelocity = prop.angularVelocity;
    }
    else
    {
        velocity.SetZero();
        angularVelocity = 0;
    }

    force.SetZero();
    torque = 0;

    {
        Shape::Attributes attr = shape->ComputeAttributes(prop.density);

        mass = attr.mass;
        massInverse = 1 / attr.mass;

        if(mass <= 0) massInverse = 0;

        inertia = attr.inertia;
        inertiaInverse = 1 / attr.inertia;
    }


}

bool Body::ComputeManifold(Body& other, Manifold& out)
{
    if(type != typeDynamic && other.type != typeDynamic) return false; // TODO - move to broadphase
    if(!shape->ComputeContact(transform, *other.shape, other.transform, out)) return false;

    out.body1 = this;
    out.body2 = &other;

    out.restitution = std::max(restitution, other.restitution);
    out.friction = 0.5;

    return true;
}

void Body::IntegrateForces(f32 deltaTime, const Vec2& gravity)
{
    if(type == typeDynamic)
    {
        velocity += (force * massInverse + gravity) * deltaTime;
        angularVelocity += torque * inertiaInverse * deltaTime;
    }
}

void Body::IntegrateVelocities(f32 deltaTime)
{
    if(type == typeDynamic)
    {
        Vec2 pos = transform.GetPosition();
        f32  rot = transform.GetRotation().ComputeAngle();

        pos += velocity * deltaTime;
        rot += angularVelocity * deltaTime;

        transform.Set(pos, Angle(rot));

    }
}

void Body::Draw(PhysicsDebugRenderer& renderer) const
{
   
    shape->Draw(renderer, transform);

    if(type == typeDynamic)
    {
        std::string str = "(" + std::to_string(velocity.x) + ", " + std::to_string(velocity.y) + ")\n" + std::to_string(angularVelocity);
        renderer.DrawText(transform.GetPosition(), str);
        renderer.DrawEdge(transform.GetPosition(), transform.GetPosition() + velocity);
    }
    
}

}