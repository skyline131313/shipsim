
#ifndef ZZ_PHYSICS_DEBUGRENDERER_H
#define ZZ_PHYSICS_DEBUGRENDERER_H

#include "../common/common.h"

namespace zz
{

class PhysicsDebugRenderer
{
public:

    virtual void DrawText(const Vec2& pos, const std::string& str) const = 0;
    virtual void DrawContact(const Vec2& pos, const Vec2& normal) const = 0;
    virtual void DrawCircle(const Transform& transform, f32 radius) const = 0;
    virtual void DrawEdge(const Vec2& v0, const Vec2& v1) const = 0;

};

}


#endif // guard