
#include "convex.h"

namespace zz
{


const Vec2& Convex::Transformed::GetSupport(const Vec2& direction) const
{
    f32 proj = v[0] * direction;
    s32 j = 0;

    for(s32 i = 1; i < num; ++i)
    {
        f32 p = v[i] * direction;

        if(p > proj)
        {
            j = i;
            proj = p;
        }
    }

    return v[j];
}

void Convex::Set(const Vec2 vertices[], s32 num)
{
    if(num < 3) return;

    // gift wrap algorithm

    s32 first = 0;

    for(s32 i = 1; i < num; ++i)
    {
             if(vertices[i].x <  vertices[first].x) first = i;
        else if(vertices[i].x == vertices[first].x)
             if(vertices[i].y <  vertices[first].y) first = i;
    }

    vertexNum = 0;

    s32 p = first;

    do
    {
        v[vertexNum] = vertices[p];

        p = 0;

        for(s32 i = 1; i < num; ++i)
        {
            Vec2 edge1 = vertices[p] - v[vertexNum];
            Vec2 edge2 = vertices[i] - v[vertexNum];

            f32 angle = edge1.PerpDot(edge2);

                 if(angle <  0) p = i;
            else if(angle == 0)
            {
                if(edge2.LengthSqr() > edge1.LengthSqr()) p = i;
            }
        }

        vertexNum++;

    } while(p != first && vertexNum < vertexMax);
    
    Vec2 center = ComputeCenter();

    for(s32 i = 0; i < vertexNum; ++i)
    {
        v[i] -= center;
    }

    for(s32 i = 0; i < vertexNum; ++i)
    {
        n[i] = (v[WrapIndex(i+1)] - v[i]).Perp();
        n[i].Normalize();

        if(n[i] * v[i] < 0) n[i] = -n[i];
    }

}

void Convex::SetAsBox(f32 w, f32 h)
{
    vertexNum = 4;

    v[0] = Vec2(-w,  h); v[1] = Vec2( w,  h);
    v[2] = Vec2( w, -h); v[3] = Vec2(-w, -h);

    n[0] = Vec2(0, 1); n[1] = Vec2( 1,0);
    n[2] = Vec2(0,-1); n[3] = Vec2(-1,0);

}

void Convex::Draw(PhysicsDebugRenderer& renderer, const Transform& transform) const
{
    Vec2 p[vertexMax];

    for(s32 i = 0; i < vertexNum; ++i) p[i] = transform.TransformPoint(v[i]);

    for(s32 i = 0; i < vertexNum; ++i)
    {
        renderer.DrawEdge(p[i], p[WrapIndex(i+1)]);
        Vec2 c = (p[i] + p[WrapIndex(i+1)]) / 2;
        renderer.DrawEdge(c, c + transform.RotatePoint(n[i]));
    }
}

Shape::Attributes Convex::ComputeAttributes(f32 density) const
{
    Attributes attr;

    f32 sum1 = 0;
    f32 sum2 = 0;

    for(s32 i = 0; i < vertexNum; ++i)
    {
        const Vec2& v0 = v[i];
        const Vec2& v1 = v[ WrapIndex(i+1) ];

        sum1 += v0.LengthSqr() + v1.LengthSqr() + v0.x*v1.x + v0.y*v1.y;
        sum2 += v0.PerpDot(v1);
    }

    attr.mass = (sum2 / 2) * density;
    attr.inertia = (attr.mass / 6) * (sum1 / sum2);
    attr.inertia = 62; // TODO TEMP...

    return attr;
}

Vec2 Convex::ComputeCenter() const
{
    Vec2 c; c.SetZero();
    f32 area = 0;

    for(s32 i = 0; i < vertexNum; ++i)
    {
        const Vec2& v0 = v[i];
        const Vec2& v1 = v[ WrapIndex(i+1) ];

        f32 a = v0.PerpDot(v1);

        c += (v0+v1) * a;
        area += a;
    }

    area /= 2;
    c /= 6 * area;

    return c;
}

}