
#include "shape.h"
#include "circle.h"
#include "convex.h"

namespace zz
{

namespace
{
    struct SEdge
    {
        Vec2 a;
        Vec2 b;
        Vec2 n;
    };

    bool ClipEdge(const Vec2& pt, const Vec2& norm, SEdge& e)
    {
        f32 d = pt * norm;

        f32 da = e.a * norm - d;
        f32 db = e.b * norm - d;

        if(db * da < 0)
        {
            (da < 0 ? e.a : e.b)  =  e.a + (e.b-e.a) * (da / (da - db));
        }
        else if(db < 0 && da < 0)
        {
            return false; // both points on wrong side of plane
        }

        return true;
    }

    std::pair<s32,f32> ComputeSeparation(const Convex::Transformed& a, const Convex::Transformed& b)
    {
        f32 distance = -std::numeric_limits<f32>::max();
        s32 j = 0;

        for(s32 i = 0; i < a.num; ++i)
        {
            const Vec2& p = b.GetSupport(-a.n[i]);
            f32 d = a.n[i] * (p - a.v[i]);

            if(d > distance)
            {
                j = i;
                distance = d;
            }
        }

        return std::make_pair(j, distance);
    }

    const Vec2& GJKSupportConvex(const Vec2 pts[], s32 num, const Vec2& direction)
    {
        s32 j = 0;
        f32 distance = pts[0] * direction;

        for(int i = 1; i < num; ++i)
        {
            f32 d = pts[i] * direction;

            if(d > distance)
            {
                j = i;
                distance = d;
            }
        }

        return pts[j];
    }

    bool SimplexLine(Vec2 pts[], s32& num, Vec2& direction)
    {
        Vec2 a = pts[1];
        Vec2 b = pts[0];

        Vec2 ba = b - a;
        Vec2 oa = -a;

        if(ba * oa > 0)
        {
            direction = ba.Perp() * ba.PerpDot(oa);
        }
        else
        {
            pts[0] = a;
            num = 1;
            direction = oa;
        }

        return false;

    }

    bool SimplexTriangle(Vec2 pts[], s32& num, Vec2& direction)
    {
        Vec2 a = pts[2];
        Vec2 b = pts[1];
        Vec2 c = pts[0];

        Vec2 ba = b - a;
        Vec2 ca = c - a;
        Vec2 oa = -a;

        // get "normal" of edges in opposite direction of the other edge of triangle
        Vec2 dba = ba.Perp() * -ba.PerpDot(ca);
        Vec2 dca = ca.Perp() * -ca.PerpDot(ba);

        // check if origin outside an edge of the triangle
        if( dca * oa > 0 )
        {
            // check if contained in line segment
            if(ca * oa > 0)
            {
                // back to a line segment case
                pts[0] = c;
                pts[1] = a;
                num = 2;

                direction = dca;
                
                return false;
            }
            else
            {
                if(ba * oa > 0)
                {
                    pts[0] = b;
                    pts[1] = a;
                    num = 2;

                    direction = dba;

                    return false;
                }
                else
                {
                    
                    pts[0] = a;
                    num = 1;

                    direction = oa;

                    return false;
                }
            }
        }
        else
        {
            if(dba * oa > 0)
            {
                if(ba * oa > 0)
                {
                    pts[0] = b;
                    pts[1] = a;
                    num = 2;

                    direction = dba;

                    return false;
                }
                else
                {
                    
                    pts[0] = a;
                    num = 1;

                    direction = oa;

                    return false;
                }
            }
            else
            {
                return true;
            }
        }


    }


    bool DoSimplex(Vec2 pts[], s32& num, Vec2& direction)
    {
        switch(num)
        {
        case 2: return SimplexLine(pts, num, direction);
        case 3: return SimplexTriangle(pts, num, direction);
        }

        return false;
    }

}

// Circle vs Circle
bool Shape::ComputeContact(const Circle& a, const Transform& at, const Circle& b, const Transform& bt, Contact& out)
{
    Vec2 displacement = at.GetPosition() - bt.GetPosition();

    f32 distance = displacement.Normalize();
    f32 totalRadius = (a.radius+b.radius)*(a.radius+b.radius);

    // circles aren't close enough to touch
    if(distance*distance > totalRadius) return false;
   
    out.contactsNum = 1;
    out.penetration = distance - (a.radius + b.radius);
    out.contacts[0] = (bt.GetPosition() + at.GetPosition()) / 2;
    out.normal = displacement;

    return true;

}

bool Shape::ComputeContact(const Convex& a, const Transform& at, const Convex& b, const Transform& bt, Contact& out)
{

#if 0
    Vec2 av[Convex::vertexMax];
    Vec2 bv[Convex::vertexMax];

    for(s32 i = 0; i < a.vertexNum; ++i) av[i] = at.TransformPoint(a.v[i]);
    for(s32 i = 0; i < b.vertexNum; ++i) bv[i] = bt.TransformPoint(b.v[i]);
    
    Vec2 pts[3];
    s32  num = 0;
    
    Vec2 direction = bt.GetPosition() - at.GetPosition();

    pts[num] = GJKSupportConvex(av, a.vertexNum, direction) - GJKSupportConvex(bv, b.vertexNum, -direction);
    direction = -pts[num];
    num++;

    while(true)
    {
        pts[num] = GJKSupportConvex(av, a.vertexNum, direction) - GJKSupportConvex(bv, b.vertexNum, -direction);

        if(pts[num] * direction < 0) return false;

        num++;

        if(DoSimplex(pts, num, direction))
        {
            // TODO - actual generation just random data for now
            out.contacts[0] = (at.GetPosition() + bt.GetPosition()) / 2; 
            out.normal = at.GetPosition() - bt.GetPosition();
            out.normal.Normalize();
            out.contactsNum = 1;

            return true;
        }
    }


    return false;
#else

    const Convex::Transformed tfa(a, at);
    const Convex::Transformed tfb(b, bt);

 
    auto resultA = ComputeSeparation(tfa, tfb);
    if(resultA.second > 0) return false;

    auto resultB = ComputeSeparation(tfb, tfa);
    if(resultB.second > 0) return false;


    const Convex::Transformed* ref;
    const Convex::Transformed* inc;
    s32 edge;

    if(resultA.second > resultB.second)
    {
        ref = &tfa;
        inc = &tfb;
        edge = resultA.first;
    }
    else
    {
        ref = &tfb;
        inc = &tfa;
        edge = resultB.first;
    }

    

    SEdge e1 = { ref->v[edge], ref->v[WrapIndex(edge+1, ref->num)], ref->n[edge] };
    SEdge e2;

    Vec2 clipNormal = e1.b - e1.a; clipNormal.Normalize();

    // get most parallel edge facing the e1 normal
    {
        f32 proj = inc->n[0] * e1.n;
        s32 j = 0;

        for(s32 i = 1; i < inc->num; ++i)
        {
            f32 p = inc->n[i] * e1.n;
            if( p < proj )
            {
                proj = p;
                j = i;
            }
        }

        e2.a = inc->v[j];
        e2.b = inc->v[WrapIndex(j+1, inc->num)];
        e2.n = inc->n[j];
    }


    if(!ClipEdge(e1.a,  clipNormal, e2)) return false;
    if(!ClipEdge(e1.b, -clipNormal, e2)) return false;

    out.contactsNum = 0;
    out.normal = -e2.n;
    out.penetration = 0;

    if( (e2.a - e1.a) * e1.n < 0 ) out.contacts[out.contactsNum++] = e2.a;
    if( (e2.b - e1.a) * e1.n < 0 ) out.contacts[out.contactsNum++] = e2.b;

    return out.contacts != 0;
#endif
}

bool Shape::ComputeContact(const Circle& a, const Transform& at, const Convex& b, const Transform& bt, Contact& out)
{
    return false;
}


}