
#ifndef ZZ_PHYSICS_SHAPES_CIRCLE_H
#define ZZ_PHYSICS_SHAPES_CIRCLE_H

#include "shape.h"

namespace zz
{

class Circle : public Shape
{
public:

    typedef Factory<Circle> Factory;

    void    Set(f32 r) { radius = r; }
    f32     GetRadius() const { return radius; }

    virtual void        Draw(PhysicsDebugRenderer& renderer, const Transform& transform) const override { renderer.DrawCircle(transform, radius); }
    virtual Attributes  ComputeAttributes(f32 density) const override;

private:

    friend class Shape;

    f32 radius;

};

}

#endif // guard