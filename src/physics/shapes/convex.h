
#ifndef ZZ_PHYSICS_SHAPES_CONVEX_H
#define ZZ_PHYSICS_SHAPES_CONVEX_H

#include "shape.h"

namespace zz
{

class Convex : public Shape
{
public:

    static const s32 vertexMax = 16;

    typedef Factory<Convex> Factory;

    struct Transformed
    {
        Vec2 v[vertexMax];
        Vec2 n[vertexMax];
        s32  num;

        Transformed(const Convex& c, const Transform& t)
            : num(c.vertexNum)
        {
            for(s32 i = 0; i < num; ++i) { v[i] = t.TransformPoint(c.v[i]); n[i] = t.RotatePoint(c.n[i]); }
        }

        const Vec2& GetSupport(const Vec2& direction) const;

    };

    void Set(const Vec2 vertices[], s32 num);
    void SetAsBox(f32 w, f32 h);

    virtual void        Draw(PhysicsDebugRenderer& renderer, const Transform& transform) const override;
    virtual Attributes  ComputeAttributes(f32 density) const override;

private:

    friend class Shape;

    Vec2 v[vertexMax];
    Vec2 n[vertexMax];
    s32  vertexNum;

    s32 WrapIndex(s32 i) const { return i < vertexNum ? i : 0; }
    Vec2 ComputeCenter() const;

};

}

#endif // guard