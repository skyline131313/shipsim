
#include "circle.h"

namespace zz
{


Shape::Attributes Circle::ComputeAttributes(f32 density) const
{
    Attributes out;

    f32 mass = density * Math::Pi<f32>() * radius * radius;
    
    out.mass = mass;
    out.inertia = (mass * radius * radius) / 2;

    return out;
}


}