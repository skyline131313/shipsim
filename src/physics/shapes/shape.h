
#ifndef ZZ_PHYSICS_SHAPES_SHAPE_H
#define ZZ_PHYSICS_SHAPES_SHAPE_H

#include "../debugrenderer.h"

#include "../../common/common.h"

namespace zz
{

class Circle;
class Convex;

class Shape
{
public:

    struct Attributes
    {
        f32 mass;
        f32 inertia;
    };
    
    struct Contact
    {
        static const s32 contactsMax = 2;

        Vec2    normal;
        Vec2    contacts[contactsMax];
        s32     contactsNum;
        f32     penetration;
    };

    template<typename S> class Factory;

    static bool ComputeContact(const Circle& a, const Transform& at, const Circle& b, const Transform& bt, Contact& out);
    static bool ComputeContact(const Convex& a, const Transform& at, const Convex& b, const Transform& bt, Contact& out);

    static bool ComputeContact(const Circle& a, const Transform& at, const Convex& b, const Transform& bt, Contact& out);

    static bool ComputeContact(const Convex& a, const Transform& at, const Circle& b, const Transform& bt, Contact& out)
    { return ComputeContact(b,bt,a,at,out); }


    virtual ~Shape() { }

    virtual bool ComputeContact(const Transform& at, const Shape&  b, const Transform& bt, Contact& out) const = 0;
    virtual bool ComputeContact(const Transform& at, const Circle& b, const Transform& bt, Contact& out) const = 0;
    virtual bool ComputeContact(const Transform& at, const Convex& b, const Transform& bt, Contact& out) const = 0;

  //virtual AABB ComputeBounds(const Transform& transform) const = 0;
    virtual void Draw(PhysicsDebugRenderer& renderer, const Transform& transform) const = 0;
    virtual Attributes ComputeAttributes(f32 density) const = 0;

    virtual std::unique_ptr<Shape> Clone() const = 0;
    
};


//! This class is a bit over the top but saves on a lot of redundant code.
//! Really all this does is override the the Shape::ComputeManifold() methods for a Shape 
//! such that the appropriate Shape::Compute() method is used.
template<typename S>
class Shape::Factory
{
public:

    static_assert(!std::is_same<Shape,S>::value,    "Cannot construct a Shape of type Shape.");
    static_assert(std::is_base_of<Shape,S>::value,  "Type must be derived from Shape.");

    typedef S ShapeType;

    static std::unique_ptr<ShapeType> Create() { return std::unique_ptr<ShapeType>(new Proxy()); }

private:

    Factory();
    ~Factory();

    class Proxy : public ShapeType
    {
        virtual bool ComputeContact(const Transform& at, const Shape&  b, const Transform& bt, Contact& out) const override
        {
            return b.ComputeContact(bt, *this, at, out);
        }

        virtual bool ComputeContact(const Transform& at, const Circle& b, const Transform& bt, Contact& out) const override
        {
            return Shape::ComputeContact(*this, at, b, bt, out);
        }

        virtual bool ComputeContact(const Transform& at, const Convex& b, const Transform& bt, Contact& out) const override
        {
            return Shape::ComputeContact(*this, at, b, bt, out);
        }

        virtual std::unique_ptr<Shape> Clone() const override { return std::unique_ptr<Shape>(new Proxy(*this)); }
    };

};

}

#endif // guard