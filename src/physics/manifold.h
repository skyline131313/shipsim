
#ifndef ZZ_PHYSICS_MANIFOLD_H
#define ZZ_PHYSICS_MANIFOLD_H

#include "../common/common.h"
#include "shapes/shape.h"

namespace zz
{

class Body;

//! \todo Fix encapsulation ...
class Manifold : public Shape::Contact
{
public:

    Body* body1;
    Body* body2;

    f32 restitution;
    f32 friction;
    
    void Draw(PhysicsDebugRenderer& renderer);

};

}

#endif // guard