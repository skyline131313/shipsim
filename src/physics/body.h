
#ifndef ZZ_PHYSICS_BODY_H
#define ZZ_PHYSICS_BODY_H

#include "../common/common.h"
#include "shapes/shape.h"
#include "manifold.h"

namespace zz
{

class IsolateSystem;

class Body
{
public:

    enum Type
    {
        typeStatic,
        typeDynamic
    };

    struct Properties : NonCopyable
    {
        Properties()
            : shape(nullptr)
            , type(typeStatic)
            , density(1)
            , restitution(0.1f)
        {
            position.SetZero();
            velocity.SetZero();
            rotation = 0;
            angularVelocity = 0;
        }

        Shape*  shape;
        Type    type;
        f32     density;
        f32     restitution;

        Vec2    position;
        Vec2    velocity;

        f32     rotation;
        f32     angularVelocity;

    };

    static void     ManifoldApplyImpulse(Manifold& m);

                    Body(IsolateSystem* system, const Properties& prop);

    IsolateSystem*  GetIsolateSystem() const { return isolateSystem; }
    const Transform& GetTransform() const { return transform; }

    bool ComputeManifold(Body& other, Manifold& out);
    void IntegrateForces(f32 deltaTime, const Vec2& gravity);
    void IntegrateVelocities(f32 deltaTime);

    void Draw(PhysicsDebugRenderer& renderer) const;

private:

    IsolateSystem* isolateSystem;

    Transform transform;

    Vec2    force;
    Vec2    velocity;

    f32     torque;
    f32     angularVelocity;

    Type    type;

    std::unique_ptr<Shape> shape;

    f32     mass;
    f32     massInverse;
    f32     inertia;
    f32     inertiaInverse;
    f32     restitution;
    f32     density;

    

};

}

#endif // guard