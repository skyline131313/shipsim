
#ifndef ZZ_PHYSICS_ISOLATESYSTEM_H
#define ZZ_PHYSICS_ISOLATESYSTEM_H

#include "../common/common.h"

#include "body.h"
#include "debugrenderer.h"

namespace zz
{

class IsolateSystem
{
public:

            IsolateSystem(const Vec2& gravity) : gravity(gravity) { }

    void    Step(f32 deltaTime, s32 iterations);

    Body*   AddBody(const Body::Properties& prop);
    bool    RemoveBody(Body* body);

    void    Draw(PhysicsDebugRenderer& renderer) const;

private:

    Vec2 gravity;

    std::list<std::unique_ptr<Body>> bodies;
    std::list<Manifold> manifolds;
};

} // ns zz


#endif // guard