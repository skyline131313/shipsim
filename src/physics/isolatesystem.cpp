
#include "isolatesystem.h"

namespace zz
{

void IsolateSystem::Step(f32 deltaTime, s32 iterations)
{
    manifolds.clear();

    for(auto i = bodies.begin(); i != bodies.end(); ++i)
    {
        for(auto j = std::next(i); j != bodies.end(); ++j)
        {
            Manifold m;

            if((*i)->ComputeManifold(*(*j), m))
            {
                manifolds.push_back(m);
            }
        }
    }

    for(auto& b : bodies)
    {
        b->IntegrateForces(deltaTime, gravity);
    }

    for(int i = 0; i < iterations; ++i)
    {
        for(auto& m : manifolds)
        {
            Body::ManifoldApplyImpulse(m);
        }
    }

    for(auto& b : bodies)
    {
        b->IntegrateVelocities(deltaTime);
    }

}

Body* IsolateSystem::AddBody(const Body::Properties& prop)
{
    bodies.push_back(std::unique_ptr<Body>(new Body(this, prop)));

    return bodies.back().get();
}

bool IsolateSystem::RemoveBody(Body* body)
{
    if(body == nullptr || body->GetIsolateSystem() != this) return false;
   
    for(auto it = bodies.begin(); it != bodies.end(); ++it)
    {
        if(it->get() == body)
        {
            bodies.erase(it);
            return true;
        }
    }

    return false;
}

void IsolateSystem::Draw(PhysicsDebugRenderer& renderer) const
{
    for(auto& b : bodies) b->Draw(renderer);

    for(auto& m : manifolds)
    {
        for(s32 i = 0; i < m.contactsNum; ++i)
        {
            renderer.DrawContact(m.contacts[i], m.normal);
        }
    }

}

}